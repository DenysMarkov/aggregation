namespace Aggregation
{
    public class Client
    {
        private readonly Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
        }

        public bool AddDeposit(Deposit deposit)
        {
            bool result = default;

            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;
                    result = true;
                    break;
                }
            }

            return result;
        }

        public decimal TotalIncome()
        {
            decimal result = default;

            foreach (var deposit in deposits)
            {
                if (deposit == null)
                {
                    break;
                }
                else
                {
                    result += deposit.Income();
                }
            }

            return result;
        }

        public decimal MaxIncome()
        {
            decimal result = deposits[0].Income();

            decimal checkingIncome;
            for (int i = 1; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    break;
                }
                else
                {
                    checkingIncome = deposits[i].Income();
                    if (checkingIncome > result)
                    {
                        result = checkingIncome;
                    }
                }
            }

            return result;
        }

        public decimal GetIncomeByNumber(int number)
        {
            decimal result;

            if (deposits[number - 1] == null)
            {
                result = 0;
            }
            else
            {
                result = deposits[number - 1].Income();
            }

            return result;
        }
    }
}