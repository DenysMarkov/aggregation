namespace Aggregation
{
    public class SpecialDeposit : Deposit
    {
        public SpecialDeposit(decimal amount, int period) : base(amount, period) { }

        public override decimal Income()
        {
            decimal result = Amount;
            decimal percent = 1.01m;

            for (int i = 0; i < Period; i++)
            {
                result *= percent;
                percent += 0.01m;
            }
            result -= Amount;

            return result;
        }
    }
}