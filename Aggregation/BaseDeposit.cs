namespace Aggregation
{
    public class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal amount, int period) : base(amount, period) { }

        public override decimal Income()
        {
            decimal result = Amount;
            decimal percent = 1.05m;

            for (int i = 0; i < Period; i++)
            {
                result *= percent;
            }
            result -= Amount;

            return result;
        }
    }
}