namespace Aggregation
{
    public class LongDeposit : Deposit
    {
        public LongDeposit(decimal amount, int period) : base(amount, period) { }

        public override decimal Income()
        {
            decimal result = Amount;

            if (Period > 6)
            {
                decimal percent = 1.15m;

                for (int i = 0; i < Period - 6; i++)
                {
                    result *= percent;
                }
            }
            result -= Amount;

            return result;
        }
    }
}